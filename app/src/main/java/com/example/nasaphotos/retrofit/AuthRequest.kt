package com.example.nasaphotos.retrofit

data class AuthRequest(val username: String, val password: String)
