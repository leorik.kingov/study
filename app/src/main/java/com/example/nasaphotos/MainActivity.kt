package com.example.nasaphotos


import android.os.Bundle
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nasaphotos.adapter.ProductAdapter
import com.example.nasaphotos.databinding.ActivityMainBinding
import com.example.nasaphotos.retrofit.AuthRequest
import com.example.nasaphotos.retrofit.MainApi
import com.example.nasaphotos.retrofit.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    private lateinit var adapter:ProductAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        adapter = ProductAdapter()
        binding.rcView.layoutManager = LinearLayoutManager(this)
        binding.rcView.adapter = adapter
        supportActionBar?.title = "Guest"
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()
        val retrofit = Retrofit.Builder()
            .baseUrl("https://dummyjson.com").client(client)
            .addConverterFactory(GsonConverterFactory.create()).build()
        val mainApi = retrofit.create(MainApi::class.java)
        var user:User? = null
//        CoroutineScope(Dispatchers.IO).launch {
//            user = mainApi.auth(
//                AuthRequest(
//                    "kminchelle","0lelplR"
//                )
//            )
//            runOnUiThread {
//                supportActionBar?.title = user?.firstName
//            }
//        }
        binding.sv.setOnQueryTextListener(object:SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }


            override fun onQueryTextChange(newText: String?): Boolean {
                CoroutineScope(Dispatchers.IO).launch {
                    val list = newText?.let { mainApi.getProductsByNameAuth(user?.token ?: "",it) }
                    runOnUiThread {
                        binding.apply {
                            adapter.submitList(list?.products)

                        }
                    }
                }
                return true
            }

        })


    }
}